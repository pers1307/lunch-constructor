export const environment = {
  production: true,
  endPoint     : 'http://api.sushnaya-express.ru',
  endPointCart : 'https://sushnaya-express.ru',
  typesUrl     : '/lunch-constructor.get-all-types',
  lunchUrl     : '/lunch-constructor.get-lunch-by-external-id',
  addLunchUrl  : '/cart/api/lunch/add'
};
