import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LunchConstructorModule } from './lunch-constructor/lunch-constructor.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LunchConstructorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
