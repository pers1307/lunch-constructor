import { Component } from '@angular/core';

@Component({
  selector: 'app-lunch-constructor',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lunch-constructor-light';
}
