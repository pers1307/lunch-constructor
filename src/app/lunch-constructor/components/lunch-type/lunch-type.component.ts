import { Component, OnInit } from '@angular/core';
import { LunchStore } from '../../services/stores/lunch-store';
import { LunchType } from '../../classes/lunch-type';

@Component({
  selector: 'app-lunch-type',
  templateUrl: './lunch-type.component.html',
  styleUrls: ['./lunch-type.component.css']
})
export class LunchTypeComponent implements OnInit {

  public allTypes: Array<LunchType> = [];
  public typeSelect = '';

  constructor(
      private lunchStore: LunchStore
  ) {

  }

  async ngOnInit() {
    this.allTypes = await this.lunchStore.getAllTypes();
    this.typeSelect = this.allTypes[0].external_id;
    await this.lunchStore.setCurrentLunchType(this.allTypes[0]);
  }

  public async onTypeChange(lunchType) {
    await this.lunchStore.setCurrentLunchType(lunchType);
  }
}
