import { Component, OnInit } from '@angular/core';
import { LunchStore } from '../../services/stores/lunch-store';

@Component({
  selector: 'app-lunch-others',
  templateUrl: './lunch-others.component.html',
  styleUrls: ['./lunch-others.component.css']
})
export class LunchOthersComponent {
  constructor(
      public lunchStore: LunchStore
  ) {

  }
}
