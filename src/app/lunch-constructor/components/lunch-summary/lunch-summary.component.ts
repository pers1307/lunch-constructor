import { Component, OnInit } from '@angular/core';
import { LunchStore } from '../../services/stores/lunch-store';
import { LunchValidator } from '../../services/validators/lunch-validator';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-lunch-summary',
  templateUrl: './lunch-summary.component.html',
  styleUrls: ['./lunch-summary.component.css'],
  animations: [
      trigger('changeVision', [
        state('block', style({
          opacity: 1
        })),
        state('none', style({
          opacity: 0
        })),
        state('fast-none', style({
          opacity: 0
        })),
        transition('*=>block', animate('10ms')),
        transition('*=>none', animate('2500ms')),
        transition('*=>fast-none', animate('10ms'))
      ])
  ]
})
export class LunchSummaryComponent implements OnInit {

  public state = 'fast-none';

  constructor(
      public lunchStore: LunchStore,
      private lunchValidator: LunchValidator
  ) {

  }

  ngOnInit() {
  }

  public async addCart() {
    if (!this.lunchValidator.validate()) {
      return;
    }

    const response: any = await this.lunchStore.sentCart();
    const eventForSent = new CustomEvent('updateCart', {'detail' : response.totalBasketCost});
    window.dispatchEvent(eventForSent);
    this.lunchStore.resetCurrentChoice();

    this.state = 'block';
    await this.delay(500);
    this.state = 'none';
  }

  protected async delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
