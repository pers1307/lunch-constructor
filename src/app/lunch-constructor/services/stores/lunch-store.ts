import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LunchType } from '../../classes/lunch-type';
import { environment } from '../../../../environments/environment';
import { Lunch } from '../../classes/lunch';
import { isNull, isNullOrUndefined } from 'util';
import { Product } from '../../classes/product';

@Injectable()
export class LunchStore {
  protected currentLunchType: LunchType = null;
  protected currentLunch: Lunch = null;
  protected lunchTypes: Array<LunchType> = [];
  protected lunches: Map<LunchType, Lunch> = new Map;

  public currentBasic: Product = null;
  public currentAddition: Product = null;
  public currentSauce: Product = null;

  constructor(
      private http: HttpClient
  ) {
  }

  public async getAllTypes() {
    if (this.lunchTypes.length === 0) {
      const response = await this.http.get(environment.endPoint + environment.typesUrl).toPromise();

      this.lunchTypes = response['data']['lunches'].map(item => {
        return new LunchType(item);
      });
    }

    return this.lunchTypes;
  }

  public async setCurrentLunchType(lunchType: LunchType) {
    this.currentLunchType = lunchType;
    this.currentLunch = await this.getLunch(lunchType);
    this.resetCurrentChoice();
  }

  public async getLunch(lunchType: LunchType) {
    let lunch = this.lunches.get(lunchType);

    if (isNullOrUndefined(lunch)) {
      const body = new FormData();
      body.append('externalId', lunchType.external_id);

      const response = await this
          .http
          .post(
              environment.endPoint + environment.lunchUrl,
              body
          )
          .toPromise();

      lunch = new Lunch(response['data']['consist']);
      this.lunches.set(lunchType, lunch);
    }

    return lunch;
  }

  public async sentCart() {
    const body = new FormData();
    body.append('type', String(this.currentLunchType.external_id));

    if (!isNull(this.currentBasic)) {
      body.append('basic', String(this.currentBasic.id));
    }

    if (!isNull(this.currentAddition)) {
      body.append('addition', String(this.currentAddition.id));
    }

    if (!isNull(this.currentSauce)) {
      body.append('sause', String(this.currentSauce.id));
    }

    return await this.http.post(environment.endPointCart + environment.addLunchUrl, body).toPromise();
  }

  public getCurrentLunchTypePrice() {
    if (!isNull(this.currentLunchType)) {
      return this.currentLunchType.price;
    }

    return 0;
  }

  public getCurrentLunchBasic() {
    if (!isNull(this.currentLunch)) {
      return this.currentLunch.basic;
    }

    return [];
  }

  public getCurrentLunchAddition() {
    if (!isNull(this.currentLunch)) {
      return this.currentLunch.addition;
    }

    return [];
  }

  public getCurrentLunchSauce() {
    if (!isNull(this.currentLunch)) {
      return this.currentLunch.sauce;
    }

    return [];
  }

  public getCurrentLunchRolls() {
    if (!isNull(this.currentLunch)) {
      return this.currentLunch.rolls;
    }

    return [];
  }

  public getCurrentLunchOther() {
    if (!isNull(this.currentLunch)) {
      return this.currentLunch.other;
    }

    return [];
  }

  public resetCurrentChoice() {
    this.currentBasic    = null;
    this.currentAddition = null;
    this.currentSauce    = null;
  }
}
