import { Injectable } from '@angular/core';
import { LunchStore } from '../stores/lunch-store';
import { isNullOrUndefined } from 'util';
import { Product } from '../../classes/product';

@Injectable()
export class LunchValidator {
  public currentBasicError: boolean = false;
  public currentAdditionError: boolean = false;
  public currentSauceError: boolean = false;

  constructor(
      protected lunchStore: LunchStore
  ) {
  }

  public validate() {
    let haveError = false;

    const countBasicChooseItem = this.lunchStore.getCurrentLunchBasic().filter(
        (product: Product) => product.choose === true
    );

    if (
        countBasicChooseItem.length > 1
        && isNullOrUndefined(this.lunchStore.currentBasic)
    ) {
      this.currentBasicError = true;
      haveError = true;
    }

    const countAdditionChooseItem = this.lunchStore.getCurrentLunchAddition().filter(
        (product: Product) => product.choose === true
    );

    if (
        countAdditionChooseItem.length > 1
        && isNullOrUndefined(this.lunchStore.currentAddition)
    ) {
      this.currentAdditionError = true;
      haveError = true;
    }

    const countSauceChooseItem = this.lunchStore.getCurrentLunchSauce().filter(
        (product: Product) => product.choose === true
    );

    if (
        countSauceChooseItem.length > 1
        && isNullOrUndefined(this.lunchStore.currentSauce)
    ) {
      this.currentSauceError = true;
      haveError = true;
    }

    if (haveError) {
      return false;
    }

    return true;
  }
}
