export class LunchType {
    constructor(object: any) {
        this.id          = object.id;
        this.external_id = object.external_id;
        this.type        = object.type;
        this.price       = object.price;
    }

    public id: number;
    public external_id: string;
    public type: string;
    public price: string;
}
