import { Product } from './product';

export class Lunch {
    constructor(object: any) {
        for (let item in object['basic']) {
            this.basic.push(new Product(object['basic'][item]));
        }

        for (let item in object['addition']) {
            this.addition.push(new Product(object['addition'][item]));
        }

        for (let item in object['sauce']) {
            this.sauce.push(new Product(object['sauce'][item]));
        }

        for (let item in object['rolls']) {
            this.rolls.push(new Product(object['rolls'][item]));
        }

        for (let item in object['other']) {
            this.other.push(new Product(object['other'][item]));
        }
    }

    public basic: Array<Product>    = [];
    public addition: Array<Product> = [];
    public sauce: Array<Product>    = [];
    public rolls: Array<Product>    = [];
    public other: Array<Product>    = [];
}
