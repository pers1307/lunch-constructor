export class Product {
    constructor(object: any) {
        this.id     = object.id;
        this.name   = object.name;
        this.choose = object.choose;
        this.price  = object.price;
        this.count  = object.count;
    }

    public id: number;
    public name: string;
    public choose: boolean;
    public price: number;
    public count: number = 0;
}
