import { Component, OnInit } from '@angular/core';
import { LunchStore } from '../../../../services/stores/lunch-store';
import { LunchValidator } from '../../../../services/validators/lunch-validator';

@Component({
  selector: 'app-lunch-addition',
  templateUrl: './lunch-addition.component.html',
  styleUrls: ['./lunch-addition.component.css']
})
export class LunchAdditionComponent implements OnInit {
  constructor(
      public lunchStore: LunchStore,
      private lunchValidator: LunchValidator
  ) {

  }

  ngOnInit() {

  }

  public onChange(product) {
    this.lunchValidator.currentAdditionError = false;
  }

}
