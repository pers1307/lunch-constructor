import { Component, OnInit } from '@angular/core';
import { LunchStore } from '../../../../services/stores/lunch-store';
import { LunchValidator } from '../../../../services/validators/lunch-validator';

@Component({
  selector: 'app-lunch-base',
  templateUrl: './lunch-base.component.html',
  styleUrls: ['./lunch-base.component.css']
})
export class LunchBaseComponent implements OnInit {
  constructor(
      public lunchStore: LunchStore,
      private lunchValidator: LunchValidator
  ) {

  }

  ngOnInit() {

  }

  public onChange(product) {
    this.lunchValidator.currentBasicError = false;
  }
}
