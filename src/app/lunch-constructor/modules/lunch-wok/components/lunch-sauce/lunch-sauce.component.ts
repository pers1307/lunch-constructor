import { Component, OnInit } from '@angular/core';
import { LunchStore } from '../../../../services/stores/lunch-store';
import { LunchValidator } from '../../../../services/validators/lunch-validator';

@Component({
  selector: 'app-lunch-sauce',
  templateUrl: './lunch-sauce.component.html',
  styleUrls: ['./lunch-sauce.component.css']
})
export class LunchSauceComponent implements OnInit {
  constructor(
      public lunchStore: LunchStore,
      private lunchValidator: LunchValidator
  ) {

  }

  ngOnInit() {

  }

  public onChange(product) {
    this.lunchValidator.currentSauceError = false;
  }

}
