import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LunchWokComponent } from './lunch-wok.component';
import { LunchBaseComponent } from './components/lunch-base/lunch-base.component';
import { LunchAdditionComponent } from './components/lunch-addition/lunch-addition.component';
import { LunchSauceComponent } from './components/lunch-sauce/lunch-sauce.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    LunchWokComponent,
    LunchBaseComponent,
    LunchAdditionComponent,
    LunchSauceComponent
  ],
  exports: [
    LunchWokComponent
  ]
})
export class LunchWokModule { }
