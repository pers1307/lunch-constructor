import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LunchConstructorComponent } from './lunch-constructor.component';
import { LunchTypeComponent } from './components/lunch-type/lunch-type.component';
import { LunchWokModule } from './modules/lunch-wok/lunch-wok.module';
import { LunchOthersComponent } from './components/lunch-others/lunch-others.component';
import { LunchSummaryComponent } from './components/lunch-summary/lunch-summary.component';
import { LunchStore } from './services/stores/lunch-store';
import { FormsModule } from '@angular/forms';
import { LunchValidator } from './services/validators/lunch-validator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    LunchWokModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  declarations: [
    LunchConstructorComponent,
    LunchTypeComponent,
    LunchOthersComponent,
    LunchSummaryComponent
  ],
  exports: [
    LunchConstructorComponent
  ],
  providers: [
    LunchStore,
    LunchValidator
  ],
})
export class LunchConstructorModule { }
